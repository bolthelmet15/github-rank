//package bolthelmet
//
//import bolthelmet.model.{ContributorsPageResponse, OrganisationId, Pagination}
//import sttp.client3.{RequestT, Response}
//import sttp.client3.asynchttpclient.zio.AsyncHttpClientZioBackend
//import TestFixture._
//import sttp.model.StatusCode
//
//object TestBackend {
//  def apply(organisationId: OrganisationId, pagination: Pagination) = AsyncHttpClientZioBackend.stub.whenRequestMatchesPartial {
//    case r if (r.uri.path == s"https://api.github.com/orgs/${organisationId.v}/members") =>
//      if (
//        r.uri.paramsMap == Map(
//          "page" -> pagination.pageNumber.v.toString,
//          "per_page" -> pagination.perPageQty.v.toString
//        )
//      ) {
//
//      }
//      Response(
//        ContributorsPageResponse(mikeAndVic),
//        StatusCode(200)
//      )
//    case RequestT(method, uri, body, headers, response, options, tags) =>
//  }
//}
