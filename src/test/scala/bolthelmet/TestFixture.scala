package bolthelmet

import bolthelmet.model.{ContributionsQty, ContributorsPageResponse, OrganisationId, Pagination, RepoCommitsPage, ReposPage, UnixTime}
import bolthelmet.services.Routes.UserTask
import bolthelmet.syntax._
import cats.syntax.either._
import org.http4s.Uri.Path.Segment
import org.http4s.{Method, Request, Uri}
import sttp.client3.{Response, ResponseException}
import sttp.model.StatusCode

object TestFixture {
  implicit val testRuntime = zio.Runtime.default

  val organisationId = OrganisationId("mighty-penguins")
  val twoOnThreePagination = Pagination(
    2.pageNumber,
    3.itemsPerPageQty
  )

  val contributorMike = "mike".contributorLogin
  val contributorVic = "vic".contributorLogin

  val repo1 = "repo1".repoName
  val repo2 = "repo2".repoName

  val mikeAndVic = contributorMike :: contributorVic :: Nil

  val typedReposPageResponse: Response[Either[ResponseException[String, io.circe.Error], ReposPage]] = Response(
    ReposPage(repo1 :: repo2 :: Nil).asRight,
    StatusCode(200)
  )

  val typedReposCommitsPageResponse: Response[Either[ResponseException[String, io.circe.Error], RepoCommitsPage]] = Response(
    RepoCommitsPage(contributorMike::Nil).asRight,
    StatusCode(200)
  )

  val unixTime = UnixTime(1)

  val request: Request[UserTask] = Request(
    Method.GET,
    Uri(
      path = Uri.Path(
        Vector(
          Segment("org"),
          Segment(organisationId.v),
          Segment("contributors")
        )
      )
    )
  )


  val rawReposResponse = Response(
    """
      |[
      |    {
      |        "name": "repo1"
      |    }
      |]
      |""".stripMargin,
    StatusCode(200)
  )

  val rawContributorsResponse = Response(
    """
      |[
      |  {
      |        "commit": {
      |            "author": {
      |                "name": "mike"
      |            }
      |         }
      |    },
      |    {
      |        "commit": {
      |            "author": {
      |                "name": "mike"
      |            }
      |        }
      |    },
      |    {
      |        "commit": {
      |            "author": {
      |                "name": "vic"
      |            }
      |        }
      |    }
      |]
      |""".stripMargin,
    StatusCode(200)
  )
  val rawEmptyResponse = Response(
    """
      |[]
      |""".stripMargin,
    StatusCode(200)
  )
}
