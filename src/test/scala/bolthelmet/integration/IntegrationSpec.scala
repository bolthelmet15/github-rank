package bolthelmet.integration

import bolthelmet.TestFixture._
import bolthelmet.di.Module
import bolthelmet.services.AppConfigImpl
import bolthelmet.syntax._
import org.http4s.Status
import org.scalatest.{FreeSpec, Matchers}
import sttp.capabilities
import sttp.capabilities.zio.ZioStreams
import sttp.client3.asynchttpclient.zio.AsyncHttpClientZioBackend
import sttp.client3.testing.SttpBackendStub
import sttp.model.Header
import zio.Task
import zio.interop.catz.taskEffectInstance

class IntegrationSpec extends FreeSpec with Matchers{

  "on /org/:org/contributors should return 200 response with contributors" in {
    val conf = AppConfigImpl(
      "test_token".githubApiAuthToken,
      2.parallelismFactor,
      100.itemsPerPageQty,
    )

    val backend: SttpBackendStub[Task, ZioStreams with capabilities.WebSockets] =
      AsyncHttpClientZioBackend.stub.whenRequestMatchesPartial {
        case r if (r.uri.toString == s"https://api.github.com/orgs/mighty-penguins/repos?page=1&per_page=2") =>
          r.headers should contain allElementsOf List(
            Header("Authorization", s"token test_token"),
            Header("Accept", "application/vnd.github.cloak-preview+json"),
          )
          rawReposResponse

        case r if (r.uri.toString == s"https://api.github.com/orgs/mighty-penguins/repos?page=2&per_page=2") =>
          rawEmptyResponse

        case r if (r.uri.toString == s"https://api.github.com/repos/mighty-penguins/repo1/commits?page=1&per_page=100") =>
          r.headers should contain allElementsOf List(
            Header("Authorization", s"token test_token"),
            Header("Accept", "application/vnd.github.cloak-preview+json"),
          )
          rawContributorsResponse

        case r if (r.uri.toString == s"https://api.github.com/repos/mighty-penguins/repo1/commits?page=2&per_page=100") =>
          rawEmptyResponse

        case r if (r.uri.toString == s"https://api.github.com/repos/mighty-penguins/repo1/commits?page=3&per_page=100") =>
          rawEmptyResponse

        case r if (r.uri.toString == s"https://api.github.com/repos/mighty-penguins/repo1/commits?page=4&per_page=100") =>
          rawEmptyResponse
      }

    val testModule = new Module(conf, backend)


    val response = testModule.routes.apply.run(request)

    val expectedBody =
      """
        |[
        |   {
        |     "name": "mike",
        |     "contributions": 2
        |   },
        |   {
        |     "name": "vic",
        |     "contributions": 1
        |   }
        |]
        |""".stripMargin

    val responseV = testRuntime.unsafeRun(response)
    responseV.status shouldBe Status.Ok
    testRuntime.unsafeRun(responseV.as[String]).replaceAll("\\s", "") shouldBe
      expectedBody.replaceAll("\\s", "")

  }

}
