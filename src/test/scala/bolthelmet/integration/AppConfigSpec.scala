package bolthelmet.integration

import bolthelmet.services.{AppConfig, AppConfigImpl}
import org.scalatest.{FreeSpec, Matchers}
import bolthelmet.syntax._

class AppConfigSpec extends FreeSpec with Matchers{

  "apply should" - {
    "read env variable" in {
      zio.Runtime.default.unsafeRun(AppConfig.apply) shouldBe AppConfigImpl(
        "test_token".githubApiAuthToken,
        4.parallelismFactor,
        100.itemsPerPageQty,
      )
    }
  }
}
