package bolthelmet.services

import bolthelmet.TestFixture._
import bolthelmet.model.{GithubException, RepoCommitsPage, ReposPage, UnixTime}
import cats.syntax.either._
import org.scalatest.{FreeSpec, Matchers}
import sttp.client3.HttpError
import sttp.model.{Header, StatusCode}
import zio.duration.Duration

class ParseGithubResponseImplSpec extends FreeSpec with Matchers {

  trait TestContext {
    val service = new ParseGithubResponseImpl
  }

  "onReposRequest should" - {
    "return OrganisationNotFound when status code is 404" in new TestContext {
      service.onReposRequest(
        organisationId,
        typedReposPageResponse.copy(code = StatusCode(404))
      ) shouldBe GithubException.OrganisationNotFound(organisationId).asLeft
    }

    "return RateLimitExceeded when rate limit exceeded" in new TestContext {
      service.onReposRequest(
        organisationId,
        typedReposPageResponse.copy(
          code = StatusCode(403),
          headers = Header("X-RateLimit-Reset", "4")::Nil
        )
      ) shouldBe GithubException.RateLimitExceeded(UnixTime(4)).asLeft
    }

    "return AbuseDetectionMechanismEngaged when Retry-After header is present and status is 403" in new TestContext {
      service.onReposRequest(
        organisationId,
        typedReposPageResponse.copy(
          code = StatusCode(403),
          headers = Header("Retry-After", "4")::Nil
        )
      ) shouldBe GithubException.AbuseDetectionMechanismEngaged(Duration.fromMillis(4000)).asLeft
    }

    "return RateLimitExceeded when both limits headers are presented" in new TestContext {
      service.onReposRequest(
        organisationId,
        typedReposPageResponse.copy(
          code = StatusCode(403),
          headers = Header("X-RateLimit-Reset", "4")::Header("Retry-After", "4")::Nil
        )
      ) shouldBe GithubException.RateLimitExceeded(UnixTime(4)).asLeft
    }

    "return Unknown when response body is left" in new TestContext {
      service.onReposRequest(
        organisationId,
        typedReposPageResponse.copy(
          body = Left(HttpError("", StatusCode(400)))
        )
      ) shouldBe GithubException.Unknown(HttpError("", StatusCode(400)).asLeft).asLeft
    }

    "return Unknown when some weird response" in new TestContext {
      service.onReposRequest(
        organisationId,
        typedReposPageResponse.copy(
          code = StatusCode(403),
          headers = Header("X-RateLimit-Reset", "4dfdf")::Nil
        )
      ).left.get.getMessage shouldBe "Unexpected github response: Right(ReposPage(List(RepoName(repo1), RepoName(repo2))))"
    }

    "return ReposPage when response body is right" in new TestContext {
      service.onReposRequest(
        organisationId,
        typedReposPageResponse
      ) shouldBe ReposPage(repo1 :: repo2 :: Nil).asRight
    }

    "return empty ReposPage when 409" in new TestContext {
      service.onReposRequest(
        organisationId,
        typedReposPageResponse.copy(code = StatusCode(409))
      ) shouldBe ReposPage(Nil).asRight
    }
  }

  "onRepoCommitsPageRequest should" - {


    "return RateLimitExceeded when rate limit exceeded" in new TestContext {
      service.onRepoCommitsPageRequest(
        typedReposCommitsPageResponse.copy(
          code = StatusCode(403),
          headers = Header("X-RateLimit-Reset", "4")::Nil
        )
      ) shouldBe GithubException.RateLimitExceeded(UnixTime(4)).asLeft
    }

    "return AbuseDetectionMechanismEngaged when Retry-After header is present and status is 403" in new TestContext {
      service.onRepoCommitsPageRequest(
        typedReposCommitsPageResponse.copy(
          code = StatusCode(403),
          headers = Header("Retry-After", "4")::Nil
        )
      ) shouldBe GithubException.AbuseDetectionMechanismEngaged(Duration.fromMillis(4000)).asLeft
    }

    "return Unknown when response body is left" in new TestContext {
      service.onRepoCommitsPageRequest(
        typedReposCommitsPageResponse.copy(
          body = Left(HttpError("", StatusCode(400)))
        )
      ) shouldBe GithubException.Unknown(HttpError("", StatusCode(400)).asLeft).asLeft
    }

    "return Unknown when some weird response" in new TestContext {
      service.onRepoCommitsPageRequest(
        typedReposCommitsPageResponse.copy(
          code = StatusCode(403),
          headers = Header("X-RateLimit-Reset", "4dfdf")::Nil
        )
      ).left.get.getMessage shouldBe "Unexpected github response: Right(RepoCommitsPage(List(ContributorLogin(mike))))"
    }

    "return RepoCommitsPage when response body is right" in new TestContext {
      service.onRepoCommitsPageRequest(
        typedReposCommitsPageResponse
      ) shouldBe RepoCommitsPage(contributorMike::Nil).asRight
    }

    "return empty RepoCommitsPage when 409" in new TestContext {
      service.onRepoCommitsPageRequest(
        typedReposCommitsPageResponse.copy(code = StatusCode(409))
      ) shouldBe RepoCommitsPage(Nil).asRight
    }
  }
}
