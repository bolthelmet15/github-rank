package bolthelmet.services

import bolthelmet.TestFixture._
import bolthelmet.model._
import bolthelmet.syntax._
import cats.syntax.either._
import org.scalamock.scalatest.MockFactory
import org.scalatest.{FreeSpec, Matchers}
import sttp.client3.HttpError
import sttp.model.StatusCode
import zio.IO
import zio.stream.ZSink

class ContributorsServiceImplSpec extends FreeSpec with Matchers with MockFactory {

  trait TestContext {
    val appConfig = AppConfigImpl(
      "tests-token".githubApiAuthToken,
      2.parallelismFactor,
      3.itemsPerPageQty
    )
    lazy val githubApiClient = mock[GithubApiClient]
    val service = new ContributorsServiceImpl(appConfig, githubApiClient)

    def pagination(pageNumber: Int, pageSize: Int) = Pagination(
      pageNumber.pageNumber,
      pageSize.itemsPerPageQty
    )
  }

  "reposPages should" - {
    "ask for repositories page by page until the empty page is returned. Page size should be parallelismFactor" in new TestContext {
      val reposPage1 = ReposPage(repo1::Nil)
      val reposPage2 = ReposPage(repo1::repo2 :: Nil)
      val reposPage3 = ReposPage(Nil)


      (githubApiClient.reposPage _).expects(organisationId, pagination(1, 2)).returning(IO.fromEither(reposPage1.asRight))
      (githubApiClient.reposPage _).expects(organisationId, pagination(2, 2)).returning(IO.fromEither(reposPage2.asRight))
      (githubApiClient.reposPage _).expects(organisationId, pagination(3, 2)).returning(IO.fromEither(reposPage3.asRight))

      testRuntime.unsafeRun(
        service.reposPages(organisationId).run(ZSink.collectAllToSet)
      ) shouldBe Set(
        reposPage1,
        reposPage2,
      )
    }

    "retry page request for 5 times if it is failing with CommunicationLayerFailure" in new TestContext {
      object TestException extends Exception

      val run = mock[()=>Either[GithubException, ReposPage]]

      override lazy val githubApiClient: GithubApiClient = new GithubApiClient {
        override def reposPage(organisationId: OrganisationId, pagination: Pagination): IO[GithubException, ReposPage] =
          IO.fromEither[GithubException, ReposPage] {
            run.apply()
          }

        override def repoCommitsPage(organisationId: OrganisationId,
                                     repoName: RepoName,
                                     pagination: Pagination): IO[GithubException, RepoCommitsPage] = ???
      }

      (run.apply _).expects().returning(GithubException.CommunicationLayerFailure(TestException).asLeft).repeated(6)
      testRuntime.unsafeRun(
        service.reposPages(organisationId).run(ZSink.collectAllToSet).either
      ) shouldBe GithubException.CommunicationLayerFailure(TestException).asLeft

    }

    "retry page request after mentioned delay times if it is failing with AbuseDetectionMechanismEngaged" in new TestContext {
      val delay = zio.duration.Duration.fromMillis(10)

      val run = mock[()=>Either[GithubException, ReposPage]]

      override lazy val githubApiClient: GithubApiClient = new GithubApiClient {
        override def reposPage(organisationId: OrganisationId, pagination: Pagination): IO[GithubException, ReposPage] =
          IO.fromEither[GithubException, ReposPage] {
            run.apply()
          }

        override def repoCommitsPage(organisationId: OrganisationId,
                                     repoName: RepoName,
                                     pagination: Pagination): IO[GithubException, RepoCommitsPage] = ???
      }

      (run.apply _).expects().returning(GithubException.AbuseDetectionMechanismEngaged(delay).asLeft)
      (run.apply _).expects().returning(ReposPage.empty().asRight)
      testRuntime.unsafeRun(
        service.reposPages(organisationId).run(ZSink.collectAllToSet)
      ) shouldBe Set()

    }

    "fail page request without retries if it is failing with RateLimitExceeded" in new TestContext {
      val run = mock[()=>Either[GithubException, ReposPage]]

      override lazy val githubApiClient: GithubApiClient = new GithubApiClient {
        override def reposPage(organisationId: OrganisationId, pagination: Pagination): IO[GithubException, ReposPage] =
          IO.fromEither[GithubException, ReposPage] {
            run.apply()
          }

        override def repoCommitsPage(organisationId: OrganisationId, repoName: RepoName, pagination: Pagination): IO[GithubException, RepoCommitsPage] = ???
      }

      (run.apply _).expects().returning(GithubException.RateLimitExceeded(2.unixTime).asLeft)
      testRuntime.unsafeRun(
        service.reposPages(organisationId).run(ZSink.collectAllToSet).either
      ) shouldBe GithubException.RateLimitExceeded(2.unixTime).asLeft
    }

    "fail page request without retries if it is failing with Unknown" in new TestContext {

      val run = mock[()=>Either[GithubException, ReposPage]]

      override lazy val githubApiClient: GithubApiClient = new GithubApiClient {
        override def reposPage(organisationId: OrganisationId, pagination: Pagination): IO[GithubException, ReposPage] =
          IO.fromEither[GithubException, ReposPage] {
            run.apply()
          }

        override def repoCommitsPage(organisationId: OrganisationId, repoName: RepoName, pagination: Pagination): IO[GithubException, RepoCommitsPage] = ???
      }

      val e = GithubException.Unknown(HttpError("",StatusCode(500)).asLeft).asLeft
      (run.apply _).expects().returning(e)
      testRuntime.unsafeRun(
        service.reposPages(organisationId).run(ZSink.collectAllToSet).either
      ) shouldBe e
    }
  }

  "commitsPages should" - {
    "ask for repo commits page chunk by page chunk (according to parallel factor in config) " +
      "until the empty page is returned" in new TestContext {
      val commitsPage1 = RepoCommitsPage(contributorMike::Nil)
      val commitsPage2 = RepoCommitsPage(contributorMike::Nil)
      val commitsPage3 = RepoCommitsPage(contributorVic::Nil)
      val commitsPage4 = RepoCommitsPage(Nil)


      (githubApiClient.repoCommitsPage _).expects(organisationId, repo1, pagination(1,3)).returning(IO.fromEither(commitsPage1.asRight))
      (githubApiClient.repoCommitsPage _).expects(organisationId, repo1, pagination(2,3)).returning(IO.fromEither(commitsPage2.asRight))
      (githubApiClient.repoCommitsPage _).expects(organisationId, repo1, pagination(3, 3)).returning(IO.fromEither(commitsPage3.asRight))
      (githubApiClient.repoCommitsPage _).expects(organisationId, repo1, pagination(4, 3)).returning(IO.fromEither(commitsPage4.asRight))
      (githubApiClient.repoCommitsPage _).expects(organisationId, repo1, pagination(5, 3)).returning(IO.fromEither(commitsPage4.asRight))
      (githubApiClient.repoCommitsPage _).expects(organisationId, repo1, pagination(6, 3)).returning(IO.fromEither(commitsPage4.asRight))

      testRuntime.unsafeRun(
        service.commitsPages(organisationId, repo1).run(ZSink.collectAllToSet)
      ) shouldBe Set(
        RepoCommitsPage(contributorMike :: contributorMike :: Nil),
        RepoCommitsPage(contributorVic :: Nil),
      )
    }

    "retry each page request for 5 times if it is failing with CommunicationLayerFailure" in new TestContext {
      object TestException extends Exception

      val run = mock[()=>Either[GithubException, RepoCommitsPage]]

      override lazy val githubApiClient: GithubApiClient = new GithubApiClient {
        override def reposPage(organisationId: OrganisationId, pagination: Pagination): IO[GithubException, ReposPage] = ???


        override def repoCommitsPage(organisationId: OrganisationId,
                                     repoName: RepoName,
                                     pagination: Pagination): IO[GithubException, RepoCommitsPage] =
          IO.fromEither[GithubException, RepoCommitsPage] {
            run.apply()
          }
      }

      (run.apply _).expects().returning(GithubException.CommunicationLayerFailure(TestException).asLeft).repeated(12)
      testRuntime.unsafeRun(
        service.commitsPages(organisationId, repo1).run(ZSink.collectAllToSet).either
      ) shouldBe GithubException.CommunicationLayerFailure(TestException).asLeft

    }

    "fail page request without retries if it is failing with RateLimitExceeded" in new TestContext {

      val run = mock[()=>Either[GithubException, RepoCommitsPage]]

      override lazy val githubApiClient: GithubApiClient = new GithubApiClient {
        override def reposPage(organisationId: OrganisationId, pagination: Pagination): IO[GithubException, ReposPage] = ???

        override def repoCommitsPage(organisationId: OrganisationId,
                                     repoName: RepoName,
                                     pagination: Pagination): IO[GithubException, RepoCommitsPage] =
          IO.fromEither[GithubException, RepoCommitsPage] {
            run.apply()
          }
      }

      (run.apply _).expects().returning(GithubException.RateLimitExceeded(2.unixTime).asLeft)
      testRuntime.unsafeRun(
        service.commitsPages(organisationId, repo1).run(ZSink.collectAllToSet).either
      ) shouldBe GithubException.RateLimitExceeded(2.unixTime).asLeft
    }

    "retry page request if it is failing with AbuseDetectionMechanismEngaged" in new TestContext {
      val delay = zio.duration.Duration.fromMillis(10)
      val e = GithubException.Unknown(HttpError("",StatusCode(500)).asLeft).asLeft
      val run = mock[()=>Either[GithubException, RepoCommitsPage]]

      override lazy val githubApiClient: GithubApiClient = new GithubApiClient {
        override def reposPage(organisationId: OrganisationId, pagination: Pagination): IO[GithubException, ReposPage] = ???

        override def repoCommitsPage(organisationId: OrganisationId,
                                     repoName: RepoName,
                                     pagination: Pagination): IO[GithubException, RepoCommitsPage] =
          IO.fromEither[GithubException, RepoCommitsPage] {
            run.apply()
          }
      }

      (run.apply _).expects().returning(GithubException.AbuseDetectionMechanismEngaged(delay).asLeft)
      (run.apply _).expects().returning(e)
      testRuntime.unsafeRun(
        service.commitsPages(organisationId, repo1).run(ZSink.collectAllToSet).either
      ) shouldBe e
    }
  }


}
