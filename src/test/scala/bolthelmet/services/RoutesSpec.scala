package bolthelmet.services

import bolthelmet.TestFixture._
import bolthelmet.model.{Contributor, GithubException}
import bolthelmet.services.Routes.UserTask
import bolthelmet.syntax._
import cats.syntax.either._
import org.http4s.Status
import org.http4s.dsl.Http4sDsl
import org.scalamock.scalatest.MockFactory
import org.scalatest.{FreeSpec, Matchers}
import sttp.client3.HttpError
import sttp.model.StatusCode
import zio.interop.catz.taskEffectInstance
import zio.{IO, ZIO}

class RoutesSpec extends FreeSpec with Matchers with MockFactory{

  trait TestContext {
    val contributors = mock[Contributors]
    val service = new RoutesImpl(contributors)

    private val dsl = Http4sDsl[UserTask]
    import zio.interop.catz._

    val routes = service.apply
  }

  "on /org/:org/contributors should" - {
    "return 200 response with contributors" in new TestContext {

      (contributors.mostActiveFirst _).expects(organisationId).returning(
        ZIO.fromFunctionM(_ => IO.fromEither[GithubException, List[Contributor]](
          List(
            Contributor(contributorMike, 5.contributionsQty),
            Contributor(contributorVic, 2.contributionsQty)
          ).asRight
        ))
      )

      val response = routes.run(request)

      val expectedBody =
        """
          |[
          |   {
          |     "name": "mike",
          |     "contributions": 5
          |   },
          |   {
          |     "name": "vic",
          |     "contributions": 2
          |   }
          |]
          |""".stripMargin

      val responseV = testRuntime.unsafeRun(response)
      responseV.status shouldBe Status.Ok
      testRuntime.unsafeRun(responseV.as[String]).replaceAll("\\s", "") shouldBe
        expectedBody.replaceAll("\\s", "")
    }

    "return 403 if rate limit exceeded" in new TestContext {
      (contributors.mostActiveFirst _).expects(organisationId).returning(
        ZIO.fromFunctionM(_ => IO.fromEither[GithubException, List[Contributor]](
          GithubException.RateLimitExceeded(1.unixTime).asLeft
        ))
      )

      val response = routes.run(request)

      val responseV = testRuntime.unsafeRun(response)
      responseV.status shouldBe Status.Forbidden
      testRuntime.unsafeRun(responseV.as[String]).replaceAll("\\s", "") shouldBe
        """
          |{"message":"Rate limit exceeded for this authorisation token. It will be reset at `UnixTime(1)`"}
          |""".stripMargin.replaceAll("\\s", "")
    }

    "return 403 if abuse detected" in new TestContext {
      val delay = zio.duration.Duration.fromMillis(10)

      (contributors.mostActiveFirst _).expects(organisationId).returning(
        ZIO.fromFunctionM(_ => IO.fromEither[GithubException, List[Contributor]](
          GithubException.AbuseDetectionMechanismEngaged(delay).asLeft
        ))
      )

      val response = routes.run(request)

      val responseV = testRuntime.unsafeRun(response)
      responseV.status shouldBe Status.Forbidden
      testRuntime.unsafeRun(responseV.as[String]).replaceAll("\\s", "") shouldBe
        """
          |{"message":"You have triggered an abuse detection mechanism for this authorisation token"}
          |""".stripMargin.replaceAll("\\s", "")
    }

    "return 404 if organisation is not found on github" in new TestContext {
      (contributors.mostActiveFirst _).expects(organisationId).returning(
        ZIO.fromFunctionM(_ => IO.fromEither[GithubException, List[Contributor]](
          GithubException.OrganisationNotFound(organisationId).asLeft
        ))
      )

      val response = routes.run(request)

      val responseV = testRuntime.unsafeRun(response)
      responseV.status shouldBe Status.NotFound
      testRuntime.unsafeRun(responseV.as[String]).replaceAll("\\s", "") shouldBe
        """
          |{"message":"Organisation `mighty-penguins` was not found on Github"}
          |""".stripMargin.replaceAll("\\s", "")
    }

    "return 500 if unknown error returned by Contributors" in new TestContext {
      val e = GithubException.Unknown(HttpError("",StatusCode(500)).asLeft).asLeft

      (contributors.mostActiveFirst _).expects(organisationId).returning(
        ZIO.fromFunctionM(_ => IO.fromEither[GithubException, List[Contributor]](
          e
        ))
      )

      val response = routes.run(request)

      val responseV = testRuntime.unsafeRun(response)
      responseV.status shouldBe Status.InternalServerError
      testRuntime.unsafeRun(responseV.as[String]).replaceAll("\\s", "") shouldBe
        """
          |{"message":"Unexpectedgithubresponse:Left(sttp.client3.HttpError:statusCode:500,response:)"}
          |""".stripMargin.replaceAll("\\s", "")
    }

    "return 500 if communication layer problems" in new TestContext {
      (contributors.mostActiveFirst _).expects(organisationId).returning(
        ZIO.fromFunctionM(_ => IO.fromEither[GithubException, List[Contributor]](
          GithubException.CommunicationLayerFailure(new Exception("hi")).asLeft
        ))
      )

      val response = routes.run(request)

      val responseV = testRuntime.unsafeRun(response)
      responseV.status shouldBe Status.InternalServerError
      testRuntime.unsafeRun(responseV.as[String]).replaceAll("\\s", "") shouldBe
        """
          |{"message":"Githubrequestfailed"}
          |""".stripMargin.replaceAll("\\s", "")
    }
  }
}
