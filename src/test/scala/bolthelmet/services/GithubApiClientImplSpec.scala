package bolthelmet.services

import bolthelmet.TestFixture._
import bolthelmet.model.{ContributionsQty, ContributorsPage, ContributorsPageResponse, GithubException, RepoCommitsPage, ReposPage}
import bolthelmet.syntax._
import cats.syntax.either._
import org.scalamock.scalatest.MockFactory
import org.scalatest.{FreeSpec, Matchers}
import sttp.capabilities
import sttp.capabilities.zio.ZioStreams
import sttp.client3.{SttpBackend, _}
import sttp.client3.asynchttpclient.zio.AsyncHttpClientZioBackend
import sttp.client3.testing.SttpBackendStub
import sttp.model.{Header, StatusCode}
import sttp.monad.MonadError
import zio.Task

class GithubApiClientImplSpec extends FreeSpec with MockFactory with Matchers {

  trait TestContext {
    def backend: SttpBackend[Task, ZioStreams with capabilities.WebSockets]

    val appConfig = AppConfigImpl(
      "test_token".githubApiAuthToken,
      15.parallelismFactor,
      15.itemsPerPageQty,
    )
    val parseGithubResponse = mock[ParseGithubResponse]
    val service = new GithunApiClientImpl(backend, appConfig, parseGithubResponse)
  }

  "reposPage should" - {
    "return repositories page according to parallel factor in config" in new TestContext {
      val rawResponse = Response(
        """
          |[
          |    {
          |        "name": "repo1"
          |    },
          |    {
          |        "name": "repo2"
          |    }
          |]
          |""".stripMargin,
        StatusCode(200)
      )

      val typedResponse: Response[Either[ResponseException[String, io.circe.Error], ReposPage]] = Response(
        ReposPage(repo1 :: repo2 :: Nil).asRight,
        StatusCode(200)
      )

      override def backend: SttpBackendStub[Task, ZioStreams with capabilities.WebSockets] =
        AsyncHttpClientZioBackend.stub.whenRequestMatchesPartial {
          case r if (r.uri.toString == s"https://api.github.com/orgs/mighty-penguins/repos?page=2&per_page=3") =>
            r.headers should contain allElementsOf List(
              Header("Authorization", s"token test_token"),
              Header("Accept", "application/vnd.github.cloak-preview+json"),
            )
            rawResponse

        }

      (parseGithubResponse.onReposRequest _).expects(organisationId, typedResponse).returning(ReposPage(repo1 :: repo2 :: Nil).asRight)

      testRuntime.unsafeRun(
        service.reposPage(
          organisationId,
          twoOnThreePagination
        )
      ) shouldBe ReposPage(
        repo1 :: repo2 :: Nil
      )

    }

    "return IO failed with GithubException.CommunicationLayerFailure if send fails" in new TestContext {

      object TestException extends Exception

      override def backend: SttpBackend[Task, ZioStreams with capabilities.WebSockets] =
        new SttpBackend[Task, ZioStreams with capabilities.WebSockets] {
          override def send[T, R >: ZioStreams with capabilities.WebSockets with capabilities.Effect[Task]](request: Request[T, R]): Task[Response[T]] =
            Task.fail(TestException)

          override def close(): Task[Unit] = ???

          override def responseMonad: MonadError[Task] = ???
        }

      testRuntime.unsafeRun(
        service.reposPage(
          organisationId,
          twoOnThreePagination
        ).either
      ) shouldBe GithubException.CommunicationLayerFailure(TestException).asLeft
    }
  }

  "repoCommitsPage should" - {
    "return contributors page with pagination according to config" in new TestContext {

      val typedResponse: Response[Either[ResponseException[String, io.circe.Error], RepoCommitsPage]] = Response(
        RepoCommitsPage(contributorMike :: contributorMike :: contributorVic :: Nil).asRight,
        StatusCode(200)
      )

      override def backend: SttpBackendStub[Task, ZioStreams with capabilities.WebSockets] =
        AsyncHttpClientZioBackend.stub.whenRequestMatchesPartial {
          case r if (r.uri.toString == s"https://api.github.com/repos/mighty-penguins/repo1/commits?page=2&per_page=3") =>
            r.headers should contain allElementsOf List(
              Header("Authorization", s"token test_token"),
              Header("Accept", "application/vnd.github.cloak-preview+json"),
            )
            rawContributorsResponse

        }

      (parseGithubResponse.onRepoCommitsPageRequest _).expects(typedResponse)
        .returning(RepoCommitsPage(contributorMike::contributorMike::contributorVic::Nil).asRight)

      testRuntime.unsafeRun(
        service.repoCommitsPage(
          organisationId,
          repo1,
          twoOnThreePagination
        )
      ) shouldBe RepoCommitsPage(contributorMike::contributorMike::contributorVic::Nil)

    }

    "return IO failed with GithubException.CommunicationLayerFailure if send fails" in new TestContext {

      object TestException extends Exception

      override def backend: SttpBackend[Task, ZioStreams with capabilities.WebSockets] =
        new SttpBackend[Task, ZioStreams with capabilities.WebSockets] {
          override def send[T, R >: ZioStreams with capabilities.WebSockets with capabilities.Effect[Task]](request: Request[T, R]): Task[Response[T]] =
            Task.fail(TestException)

          override def close(): Task[Unit] = ???

          override def responseMonad: MonadError[Task] = ???
        }

      testRuntime.unsafeRun(
        service.repoCommitsPage(
          organisationId,
          repo1,
          twoOnThreePagination
        ).either
      ) shouldBe GithubException.CommunicationLayerFailure(TestException).asLeft
    }
  }

}
