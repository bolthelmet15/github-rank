package bolthelmet.services

import bolthelmet.TestFixture._
import bolthelmet.model.{Contributor, RepoCommitsPage, ReposPage}
import bolthelmet.syntax._
import org.scalamock.scalatest.MockFactory
import org.scalatest.{FreeSpec, Matchers}
import zio.stream.ZStream

class ContributorsSpec extends FreeSpec with Matchers with MockFactory {

  trait TestContext {
    val contributorsService = mock[ContributorsService]
    val service = new ContributorsImpl(contributorsService)
  }

  "apply should" - {
    "flatmap repos pages stream to committers stream, then sort committers by their contributions QTYs" in new TestContext {
      val repo3 = "repo3".repoName
      val reposPage1 = ReposPage(repo1 :: repo2 :: Nil)
      val reposPage2 = ReposPage(repo3 :: Nil)

      val repo1CommitsPage1 = RepoCommitsPage(contributorMike :: contributorVic :: contributorMike :: Nil)
      val repo1CommitsPage2 = RepoCommitsPage(contributorMike :: contributorMike :: Nil)
      val repo2CommitsPage1 = RepoCommitsPage(contributorVic :: Nil)
      val repo3CommitsPage1 = RepoCommitsPage(Nil)

      (contributorsService.reposPages _).expects(organisationId)
        .returning(
          ZStream.fromIterable(
            reposPage1 :: reposPage2 :: Nil
          )
        )
      (contributorsService.commitsPages _).expects(organisationId, repo1)
        .returning(
          ZStream.fromIterable(
            repo1CommitsPage1 :: repo1CommitsPage2 :: Nil
          )
        )
      (contributorsService.commitsPages _).expects(organisationId, repo2)
        .returning(
          ZStream.fromIterable(
            repo2CommitsPage1 :: Nil
          )
        )
      (contributorsService.commitsPages _).expects(organisationId, repo3)
        .returning(
          ZStream.fromIterable(
            repo3CommitsPage1 :: Nil
          )
        )

      testRuntime.unsafeRun(
        service.mostActiveFirst(organisationId)
      ) shouldBe List(
        Contributor(contributorMike, 4.contributionsQty),
        Contributor(contributorVic, 2.contributionsQty),
      )
    }
  }
}
