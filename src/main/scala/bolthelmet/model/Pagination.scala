package bolthelmet.model

case class Pagination(pageNumber: PageNumber, perPageQty: ItemsPerPageQty)

case class PageNumber(v: Int) extends AnyVal
case class ItemsPerPageQty(v: Int) extends AnyVal


object PageNumber {
  implicit class PageNumberOps(p: PageNumber) {
    def +(v:Int): PageNumber = PageNumber(p.v + v)
    def next: PageNumber = PageNumber(p.v + 1)
  }
}