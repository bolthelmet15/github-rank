package bolthelmet.model

import io.circe.Decoder

case class ReposPage(repos: List[RepoName])

object ReposPage {
  implicit val decoder: Decoder[ReposPage] = _.as[List[RepoName]].map(ReposPage(_))

  implicit val empty: Empty[ReposPage] = () => ReposPage(Nil)
}