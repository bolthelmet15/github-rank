package bolthelmet.model

import io.circe.Decoder

case class ContributorsPageResponse(contributors: List[ContributorLogin])
object ContributorsPageResponse {
  implicit val decoder: Decoder[ContributorsPageResponse] =
    _
      .as[List[ContributorLogin]]
      .map(ContributorsPageResponse(_))
}