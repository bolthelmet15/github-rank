package bolthelmet.model

import io.circe.{Encoder, Json}

case class ErrorResponse(msg: String)
object ErrorResponse {
  implicit val encoder: Encoder[ErrorResponse] = m => Json.obj(
    "message" -> Json.fromString(m.msg)
  )
}
