package bolthelmet.model

import io.circe.Decoder

case class RepoName(v: String) extends AnyVal

case object RepoName {
  implicit val decoder: Decoder[RepoName] = _.downField("name").as[String].map(RepoName(_))
}
