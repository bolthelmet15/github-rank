package bolthelmet.model

import sttp.client3.ResponseException

import zio.duration.Duration

sealed trait GithubException extends Exception
object GithubException {
  case class OrganisationNotFound(organisationId: OrganisationId) extends GithubException
  case class RateLimitExceeded(resetIn: UnixTime) extends GithubException
  case class AbuseDetectionMechanismEngaged(retryIn: Duration) extends GithubException
  case class Unknown[T](githubResponse:  Either[ResponseException[String, io.circe.Error], T]) extends GithubException {
    override def getMessage: String = s"Unexpected github response: $githubResponse"
  }
  case class CommunicationLayerFailure(t: Throwable) extends GithubException {
    override def getCause: Throwable = t
    override def getMessage: String = "Github request failed"
  }
}
