package bolthelmet.model

import io.circe.Decoder

case class ContributorLogin(v: String) extends AnyVal

object ContributorLogin {
  implicit val decoder: Decoder[ContributorLogin] =
    _
      .downField("commit")
      .downField("author")
      .downField("name")
      .as[String]
      .map(ContributorLogin(_))
}