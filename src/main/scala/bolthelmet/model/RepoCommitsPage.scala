package bolthelmet.model

import io.circe.Decoder

case class RepoCommitsPage(commitsAuthors: List[ContributorLogin])
object RepoCommitsPage {
  implicit val decoder: Decoder[RepoCommitsPage] = _.as[List[ContributorLogin]].map(RepoCommitsPage(_))

  implicit val empty: Empty[RepoCommitsPage] = () => RepoCommitsPage(Nil)
}
