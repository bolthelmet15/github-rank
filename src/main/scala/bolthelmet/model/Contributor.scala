package bolthelmet.model

import io.circe.{Encoder, Json}

case class Contributor(login: ContributorLogin, contributionsQty: ContributionsQty)
object Contributor {
  implicit val encoder: Encoder[Contributor] = (a: Contributor) => Json.obj(
    "name" -> Json.fromString(a.login.v),
    "contributions" -> Json.fromInt(a.contributionsQty.v)
  )
}
