package bolthelmet.model

case class ContributorsPage(organisationId: OrganisationId, contributors: List[ContributorLogin])
