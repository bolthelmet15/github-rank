package bolthelmet.model

trait Empty[T] {
  def apply(): T
}

object Empty {
  def apply[T](implicit empty: Empty[T]) = empty.apply()
}
