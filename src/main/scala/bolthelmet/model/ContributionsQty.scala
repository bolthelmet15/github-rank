package bolthelmet.model

import io.circe.{Decoder, HCursor}

case class ContributionsQty(v: Int) extends AnyVal
object ContributionsQty {
  implicit val decoder: Decoder[ContributionsQty] = (c: HCursor) =>
    c
      .downField("total_count")
      .as[Int]
      .map(ContributionsQty(_))
}

