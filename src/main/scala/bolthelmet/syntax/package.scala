package bolthelmet

import bolthelmet.model._

package object syntax {

  implicit class StringOps(v: String) {
    def githubApiAuthToken: GithubApiAuthToken = GithubApiAuthToken(v)
    def organisationId: OrganisationId = OrganisationId(v)
    def contributorLogin: ContributorLogin = ContributorLogin(v)
    def repoName: RepoName = RepoName(v)
  }

  implicit class IntOps(v: Int) {
    def pageNumber: PageNumber = PageNumber(v)
    def itemsPerPageQty: ItemsPerPageQty = ItemsPerPageQty(v)
    def parallelismFactor: ParallelismFactor = ParallelismFactor(v)
    def unixTime: UnixTime = UnixTime(v)
    def contributionsQty: ContributionsQty = ContributionsQty(v)
  }

  implicit class GithubExceptionsOps(v: GithubException) {
    def isRetryable: Boolean = v match {
      case GithubException.CommunicationLayerFailure(_) => true
      case _ => false
    }
  }
}
