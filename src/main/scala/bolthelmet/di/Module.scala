package bolthelmet.di

import bolthelmet.services._
import com.softwaremill.macwire._
import sttp.capabilities
import sttp.capabilities.zio.ZioStreams
import sttp.client3.SttpBackend
import zio.Task

class Module(appConfig: AppConfig, sttpBe: SttpBackend[Task, ZioStreams with capabilities.WebSockets]) {

  val parseGithubResponse = wire[ParseGithubResponseImpl]
  val githubApiClient = wire[GithunApiClientImpl]
  val contributorsService = wire[ContributorsServiceImpl]
  val contributors = wire[ContributorsImpl]
  val routes = wire[RoutesImpl]

}
