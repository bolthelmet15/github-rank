package bolthelmet.services

import bolthelmet.model.{ErrorResponse, GithubException}
import bolthelmet.services.Routes.{AppEnv, UserTask}
import bolthelmet.syntax._
import cats.data._
import cats.effect.ConcurrentEffect
import cats.{Applicative, Defer}
import io.circe.Encoder
import io.circe.syntax._
import zio.interop.catz._
import org.http4s.circe._
import org.http4s.dsl.Http4sDsl
import org.http4s.implicits._
import org.http4s.{HttpRoutes, Request, Response}
import zio._
import zio.blocking.Blocking
import zio.clock.Clock


trait Routes {
  def apply(
             implicit applicative: Applicative[UserTask],
             defer: Defer[UserTask],
             concurrentEffect: ConcurrentEffect[UserTask]
           ): Kleisli[UserTask, Request[UserTask], Response[UserTask]]
}

object Routes {
  type AppEnv = Any with Clock with Blocking
  type UserTask[A] = RIO[AppEnv, A]
}

class RoutesImpl(contributors: Contributors) extends Routes {

  private val dsl = Http4sDsl[UserTask]

  import dsl._

  override def apply(
                      implicit applicative: Applicative[UserTask],
                      defer: Defer[UserTask],
                      concurrentEffect: ConcurrentEffect[UserTask]
                    ): Kleisli[UserTask, Request[UserTask], Response[UserTask]] = {

    val anonymousRoutes: HttpRoutes[UserTask] = HttpRoutes.of[UserTask] {
      case GET -> Root / "org" / organisationId / "contributors" =>
        responseBy(
          contributors.mostActiveFirst(organisationId.organisationId)
        )
    }

    anonymousRoutes.orNotFound
  }

  private def responseBy[A: Encoder](res: ZIO[AppEnv, GithubException, A]): UserTask[Response[UserTask]] = res.foldM(
    routesExceptionToErrorResponse,
    entity => Ok(entity.asJson)
  )

  private def routesExceptionToErrorResponse(e: GithubException): UserTask[Response[UserTask]] = e match {
    case GithubException.AbuseDetectionMechanismEngaged(_) =>
      Forbidden(ErrorResponse(s"You have triggered an abuse detection mechanism for this authorisation token").asJson)
    case GithubException.RateLimitExceeded(resetAt) =>
      Forbidden(ErrorResponse(s"Rate limit exceeded for this authorisation token. It will be reset at `${resetAt}`").asJson)
    case GithubException.OrganisationNotFound(organisationId) =>
      NotFound(ErrorResponse(s"Organisation `${organisationId.v}` was not found on Github").asJson)
    case e@GithubException.Unknown(_) => InternalServerError(ErrorResponse(e.getMessage).asJson)
    case e@GithubException.CommunicationLayerFailure(_) => InternalServerError(ErrorResponse(e.getMessage).asJson)
  }
}