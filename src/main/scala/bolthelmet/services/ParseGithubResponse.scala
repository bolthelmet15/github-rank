package bolthelmet.services

import java.util.concurrent.TimeUnit

import bolthelmet.model._
import bolthelmet.services.ParseGithubResponse.GithubResponse
import cats.syntax.either._
import io.circe
import sttp.client3.{Response, ResponseException}
import sttp.model.Header
import zio.duration.Duration

import scala.util.Try

trait ParseGithubResponse {
  def onReposRequest(organisationId: OrganisationId,
                     response: GithubResponse[ReposPage]): Either[GithubException, ReposPage]

  def onRepoCommitsPageRequest(response: GithubResponse[RepoCommitsPage]): Either[GithubException, RepoCommitsPage]
}

class ParseGithubResponseImpl extends ParseGithubResponse {
  val XRateLimitReset = "X-RateLimit-Reset"
  val RetryAfter = "Retry-After"

  override def onReposRequest(organisationId: OrganisationId,
                              response: GithubResponse[ReposPage]): Either[GithubException, ReposPage] = {
    response match {
      case Response(_, code, _, _, _, _) if code.code == 404 => GithubException.OrganisationNotFound(organisationId).asLeft
      case r => parseCommon(r)
    }
  }

  override def onRepoCommitsPageRequest(response: GithubResponse[RepoCommitsPage]): Either[GithubException, RepoCommitsPage] =
    parseCommon(response)

  private def parseCommon[T: Empty](response: GithubResponse[T]): Either[GithubException, T] = {
    response match {
      case r if r.code.code == 403 =>
        exceptionsOnForbidden(r).asLeft
      case Response(Right(t), code, _, headers, _, _) if code.isSuccess =>
        t.asRight
      case Response(_, code, _, _, _, _) if code.code == 409 => Empty[T].asRight
      case Response(re, _, _, _, _, _) =>
        GithubException.Unknown(re).asLeft
    }
  }

  private def exceptionsOnForbidden[T](r: GithubResponse[T]) = {
    (rateLimitResetIn(r.headers), retryAfter(r.headers)) match {
      case (Some(resetIn), _) => GithubException.RateLimitExceeded(resetIn)
      case (None, Some(duration)) => GithubException.AbuseDetectionMechanismEngaged(duration)
      case (None, None) => GithubException.Unknown(r.body)
    }
  }

  private def rateLimitResetIn(headers: Seq[Header]) =
    headers.find(_.name == XRateLimitReset).flatMap { h =>
      Try(h.value.toLong).toOption.map(UnixTime)
    }

  private def retryAfter(headers: Seq[Header]) =
    headers.find(_.name == RetryAfter).flatMap { h =>
      Try(h.value.toLong).toOption.map { secondsQty =>
        Duration(secondsQty, TimeUnit.SECONDS)
      }
    }
}

object ParseGithubResponse {
  type GithubResponse[T] = Response[Either[ResponseException[String, circe.Error], T]]
}