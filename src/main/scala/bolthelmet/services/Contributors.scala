package bolthelmet.services

import bolthelmet.model._
import bolthelmet.services.Routes.AppEnv
import bolthelmet.syntax._
import zio.ZIO
import zio.stream.ZSink

trait Contributors {
  def mostActiveFirst(organisationId: OrganisationId): ZIO[AppEnv, GithubException, List[Contributor]]
}

class ContributorsImpl(contributorsService: ContributorsService) extends Contributors {
  override def mostActiveFirst(organisationId: OrganisationId): ZIO[AppEnv, GithubException, List[Contributor]] = {
    contributorsService
      .reposPages(organisationId)
      .mapM(commitAuthors(organisationId))
      .run(ZSink.collectAllToSet)
      .map(_.toList.flatten)
      .map(groupAndSortByEntriesQtyDescenting)
  }

  private def groupAndSortByEntriesQtyDescenting(commitAuthors: List[ContributorLogin]) =
    commitAuthors.groupBy(identity).map { case (k, v) =>
      k -> v.size
    }
      .toList
      .sortBy(_._2)(Ordering.Int.reverse)
      .map { case (login, contributionsQty) =>
        Contributor(
          login,
          contributionsQty.contributionsQty
        )
      }


  private def commitAuthors(organisationId: OrganisationId)(reposPage: ReposPage) = {
    ZIO.collectAllPar(
      reposPage.repos.map{ repo =>
        contributorsService
          .commitsPages(organisationId, repo)
          .map(_.commitsAuthors)
          .run(ZSink.collectAllToSet)
          .map(_.toList.flatten)
      }
    ).map(_.flatten)
  }
}