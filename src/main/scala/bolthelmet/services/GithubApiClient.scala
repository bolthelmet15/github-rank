package bolthelmet.services

import bolthelmet.model._
import bolthelmet.services.Routes.AppEnv
import com.typesafe.scalalogging.LazyLogging
import io.circe
import sttp.capabilities
import sttp.capabilities.zio.ZioStreams
import sttp.client3._
import sttp.client3.circe.asJson
import sttp.model.Header
import zio.blocking.Blocking
import zio.duration.Duration
import zio.{IO, Task, ZIO}

trait GithubApiClient {
  def reposPage(organisationId: OrganisationId, pagination: Pagination): ZIO[AppEnv, GithubException, ReposPage]

  def repoCommitsPage(organisationId: OrganisationId,
                      repoName: RepoName,
                      pagination: Pagination): ZIO[AppEnv, GithubException, RepoCommitsPage]
}

class GithunApiClientImpl(sttpBackend: SttpBackend[Task, ZioStreams with capabilities.WebSockets],
                          appConfig: AppConfig,
                          parseResponse: ParseGithubResponse) extends GithubApiClient {
  private val headers = List(
    Header("Authorization", s"token ${appConfig.authorizationToken.v}"),
    Header("Accept", "application/vnd.github.cloak-preview+json"),
  )

  val pauseBeforeRequest = Duration.fromMillis(0)

  override def reposPage(organisationId: OrganisationId, pagination: Pagination): ZIO[AppEnv, GithubException, ReposPage] = {
    val request: RequestT[Identity, Either[ResponseException[String, circe.Error], ReposPage], Any] = basicRequest.get(
      uri"https://api.github.com/orgs/${organisationId.v}/repos".addParams(
        "page" -> pagination.pageNumber.v.toString,
        "per_page" -> pagination.perPageQty.v.toString
      )
    )
      .headers(headers: _*)
      .response(asJson[ReposPage])

      request
        .send(sttpBackend)
        .catchAll { t =>
          IO.fail(GithubException.CommunicationLayerFailure(t))
        }
        .flatMap {
          r =>
            IO.fromEither(
              parseResponse.onReposRequest(organisationId, r)
            )
        }.delay(pauseBeforeRequest)

  }


  override def repoCommitsPage(organisationId: OrganisationId,
                               repoName: RepoName,
                               pagination: Pagination): ZIO[AppEnv, GithubException, RepoCommitsPage] = {
    val request: RequestT[Identity, Either[ResponseException[String, circe.Error], RepoCommitsPage], Any] = basicRequest.get(
      uri"https://api.github.com/repos/${organisationId.v}/${repoName.v}/commits".addParams(
        "page" -> pagination.pageNumber.v.toString,
        "per_page" -> pagination.perPageQty.v.toString
      )
    )
      .headers(headers: _*)
      .response(asJson[RepoCommitsPage])

      request
        .send(sttpBackend)
        .catchAll { t =>
          IO.fail(GithubException.CommunicationLayerFailure(t))
        }
        .flatMap {
          r =>
            IO.fromEither(
              parseResponse.onRepoCommitsPageRequest(r)
            )
        }
        .delay(pauseBeforeRequest)
  }

}