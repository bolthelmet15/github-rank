package bolthelmet.services

import bolthelmet.model._
import bolthelmet.services.Routes.AppEnv
import bolthelmet.syntax._
import cats.syntax.option._
import zio.duration.Duration
import zio.stream.ZStream
import zio.{Schedule, ZIO}

trait ContributorsService {
  def reposPages(organisationId: OrganisationId): ZStream[AppEnv, GithubException, ReposPage]

  def commitsPages(organisationId: OrganisationId, repoName: RepoName): ZStream[AppEnv, GithubException, RepoCommitsPage]
}

class ContributorsServiceImpl(appConfig: AppConfig,
                              githubApiClient: GithubApiClient) extends ContributorsService {
  private val parallelismFactor = appConfig.maxParallelRequestsToGithubQty
  private val pageSize = appConfig.pageSize

  override def reposPages(organisationId: OrganisationId): ZStream[AppEnv, GithubException, ReposPage] =
    ZStream.unfoldM(1.pageNumber) { pageNumber =>
      reposPageWithRetries(organisationId, pageNumber).map {
        case ReposPage(Nil) => None
        case p => (p, pageNumber.next).some
      }
    }

  private def reposPageWithRetries(organisationId: OrganisationId, pageNumber: PageNumber) =
    withRetries(
      githubApiClient.reposPage(
        organisationId,
        Pagination(
          pageNumber,
          parallelismFactor.v.itemsPerPageQty
        )
      )
    )

  override def commitsPages(organisationId: OrganisationId, repoName: RepoName): ZStream[AppEnv, GithubException, RepoCommitsPage] =
    ZStream.unfoldM(1.pageNumber) { pageNumber =>
      val finalPageInAChunk = (pageNumber.v + parallelismFactor.v - 1).pageNumber
      ZIO.collectAllPar(
        (pageNumber.v to finalPageInAChunk.v).map { pageN =>
          commitsPageWithRetries(organisationId, repoName, pageN.pageNumber).map(_.commitsAuthors)
        }
      )
        .map(_.flatten.toList)
        .map {
          case Nil => None
          case committers => (RepoCommitsPage(committers), finalPageInAChunk.next).some
        }
    }

  private def commitsPageWithRetries(organisationId: OrganisationId,
                                     repoName: RepoName,
                                     pageNumber: PageNumber) =
    withRetries(
      githubApiClient.repoCommitsPage(
        organisationId,
        repoName,
        Pagination(
          pageNumber,
          pageSize
        )
      )
    )

  private def withRetries[T](f: ZIO[AppEnv, GithubException, T]): ZIO[AppEnv, GithubException, T] = {
    val retrySchedule = Schedule.spaced(
      Duration.fromMillis(100)).&&(Schedule.recurs(5).&&(Schedule.recurWhile[GithubException](_.isRetryable))
    )

    f.catchSome {
      case GithubException.AbuseDetectionMechanismEngaged(retryIn) => f.delay(retryIn)
    }.retry(retrySchedule)
  }
}
