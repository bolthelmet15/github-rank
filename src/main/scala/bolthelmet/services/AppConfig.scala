package bolthelmet.services

import bolthelmet.model.{GithubApiAuthToken, ItemsPerPageQty, ParallelismFactor}
import pureconfig.ConfigSource
import zio.Task
import bolthelmet.syntax._
import pureconfig._
import pureconfig.generic.auto._

trait AppConfig {
  def authorizationToken: GithubApiAuthToken
  def maxParallelRequestsToGithubQty: ParallelismFactor
  def pageSize: ItemsPerPageQty
}

case class AppConfigImpl(authorizationToken: GithubApiAuthToken,
                         maxParallelRequestsToGithubQty: ParallelismFactor,
                         pageSize: ItemsPerPageQty) extends AppConfig

object AppConfig{
  def apply: Task[AppConfig] = Task{
    val temp = ConfigSource.default.loadOrThrow[AppConfigUtil]
    AppConfigImpl(
      temp.authorizationToken,
      temp.parallelismFactor.getOrElse(10.parallelismFactor),
      temp.pageSize.getOrElse(100.itemsPerPageQty)
    )
  }

  case class AppConfigUtil(
                            authorizationToken: GithubApiAuthToken,
                            parallelismFactor: Option[ParallelismFactor],
                            pageSize: Option[ItemsPerPageQty]
                          )
}