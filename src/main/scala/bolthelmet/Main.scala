package bolthelmet

import bolthelmet.di.Module
import bolthelmet.services.Routes.{AppEnv, UserTask}
import bolthelmet.services.AppConfig
import org.http4s.server.blaze._
import sttp.capabilities
import sttp.capabilities.zio.ZioStreams
import sttp.client3.SttpBackend
import sttp.client3.asynchttpclient.zio.AsyncHttpClientZioBackend
import zio._
import zio.interop.catz._

import scala.concurrent.ExecutionContext.global
import scala.concurrent.duration.Duration

object Main extends zio.App {

  def run(args: List[String]): URIO[ZEnv, ExitCode] = {

    import zio.interop.catz.implicits._

    def server(appConfig: AppConfig,
               sttpBe: SttpBackend[Task, ZioStreams with capabilities.WebSockets]): URIO[AppEnv, ExitCode] =
      ZIO.runtime[AppEnv].flatMap{ implicit rts =>
        BlazeServerBuilder[UserTask](global)
          .withResponseHeaderTimeout(Duration.Inf)
          .bindHttp(8080, "localhost")
          .withHttpApp(new Module(appConfig, sttpBe).routes.apply)
          .serve
          .compile
          .drain
          .fold(_ => ExitCode.failure, _ => ExitCode.success)
      }

    (for {
      conf <- AppConfig.apply
      sttpBe <- AsyncHttpClientZioBackend()
      s <- server(conf, sttpBe)
    } yield s).catchAll { t =>
      URIO.apply {
        ExitCode.failure
      }
    }
  }
}