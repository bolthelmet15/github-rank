User Manual.
0. Enter project dir.
1. Use `.env` file to setup `GH_TOKEN` ENV variable, representing your Github authorisation token.
2. Execute `sbt run` to start the server.
3. Use Postman to execute `GET http://localhost:8080/org/zio/contributors` . Here instead of `zio` feel free to use Github id of your organisation. N.B. For larger organisations using this endpoint triggers Github "abuse detection mechanism". I was hitting this wall before reaching 5000 requests per hour.
