name := "github-rank"

version := "0.1"

scalaVersion := "2.12.6"

scalacOptions += "-Ypartial-unification"

resolvers += Resolver.sonatypeRepo("snapshots")

val http4sVersion = "1.0.0-M5"
val circeVersion = "0.10.0"

Test / fork := true

Test / envVars := Map("GH_TOKEN" -> "test_token")

libraryDependencies ++= Seq(
  "org.http4s" %% "http4s-dsl" % http4sVersion,
  "org.http4s" %% "http4s-blaze-server" % http4sVersion,
  "org.http4s" %% "http4s-circe" % http4sVersion,
  "com.softwaremill.macwire" %% "macros" % "2.3.6" % "provided",
  "com.softwaremill.macwire" %% "macrosakka" % "2.3.6" % "provided",
  "com.softwaremill.macwire" %% "util" % "2.3.6",
  "com.softwaremill.macwire" %% "proxy" % "2.3.6",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2",
  "dev.zio" %% "zio" % "1.0.0-RC21-2",
  "dev.zio" %% "zio-interop-cats" % "2.2.0.1",
  "dev.zio" %% "zio-streams" % "1.0.3",
  "org.scalatest" %% "scalatest" % "3.0.5" % Test,
  "org.scalamock" %% "scalamock" % "4.1.0" % Test,
  "com.softwaremill.sttp.client3" %% "core" % "3.0.0-RC8",
  "com.softwaremill.sttp.client3" %% "async-http-client-backend-zio" % "3.0.0-RC8",
  "com.softwaremill.sttp.client3" %% "circe" % "3.0.0-RC9",
  "com.github.pureconfig" %% "pureconfig" % "0.14.0",
  "ch.qos.logback" % "logback-classic" % "1.2.3",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2"
) ++ Seq(
  "io.circe" %% "circe-core",
  "io.circe" %% "circe-generic",
  "io.circe" %% "circe-parser"
).map(_ % circeVersion)